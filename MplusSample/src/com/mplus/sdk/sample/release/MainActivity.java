package com.mplus.sdk.sample.release;

import com.mapps.android.view.AdInterstitialView;
import com.mapps.android.view.AdView;
import com.mz.common.listener.AdListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements AdListener {

	private Handler handler = new Handler();
	private AdInterstitialView m_interView = null;
	private EndingDialog mEndingDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		((TextView) findViewById(R.id.version)).setText("Sample");

		btnSetting();

	}

	private void btnSetting() {
		Button btn_banner1 = (Button) findViewById(R.id.btn_banner);
		btn_banner1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				bannerView(100, 200, 300, true, AdView.TYPE_HTML);
			}
		});
		btn_banner1.setVisibility(View.GONE);
		Button btn_interstital1 = (Button) findViewById(R.id.btn_interstital);
		btn_interstital1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				interView(100, 200, 301);
			}
		});
		btn_interstital1.setVisibility(View.GONE);

		Button btn_ending1 = (Button) findViewById(R.id.btn_ending);
		btn_ending1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				endingView(100, 200, 302, AdView.TYPE_HTML);
			}
		});
		btn_ending1.setVisibility(View.GONE);

		Button btn_movie1 = (Button) findViewById(R.id.btn_movie);
		btn_movie1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				playerView(102, 202, 317);
			}
		});
		btn_movie1.setVisibility(View.VISIBLE);
	}

	private void interView(int p, int m, int s) {
		if (m_interView == null) {
			m_interView = new AdInterstitialView(MainActivity.this);
			m_interView.setAdListener(MainActivity.this);
			m_interView.setViewStyle(AdInterstitialView.VIEWSTYLE.NONE);
			m_interView.setUserAge("22");
			m_interView.setUserGender("1");
			m_interView.setLoaction(true);
			m_interView.setAccount("test");
			m_interView.setEmail("test@mezzomediaco.kr");
			m_interView.setUserAgeLevel("0");
			m_interView.setStoreurl("http://store.url");
			m_interView.setKeyword("");
			m_interView.setExternal("");
		}
		m_interView.setAdViewCode(String.valueOf(p), String.valueOf(m), String.valueOf(s));
		m_interView.ShowInterstitialView();
	}

	private void bannerView(final int p, final int m, final int s, final boolean isAni, final int type) {
		new Thread(new Runnable() {
			public void run() {
				Intent intent = new Intent(MainActivity.this, BannerSample.class);
				intent.putExtra(Constants.P, String.valueOf(p));
				intent.putExtra(Constants.M, String.valueOf(m));
				intent.putExtra(Constants.S, String.valueOf(s));
				intent.putExtra(Constants.ANI, isAni);
				intent.putExtra(Constants.TYPE, type);
				startActivity(intent);
			}
		}).start();

	}

	private void endingView(final int p, final int m, final int s, int media) {
		mEndingDialog = new EndingDialog(MainActivity.this, "테스트 종료베너", leftClickListener2, rightClickListener2);
		mEndingDialog.setCode(p, m, s, media);
		mEndingDialog.show();
	}

	private void playerView(final int p, final int m, final int s) {
		new Thread(new Runnable() {

			public void run() {
				Intent intent = new Intent(MainActivity.this, PlayerSample.class);
				intent.putExtra(Constants.P, String.valueOf(p));
				intent.putExtra(Constants.M, String.valueOf(m));
				intent.putExtra(Constants.S, String.valueOf(s));
				startActivity(intent);
			}
		}).start();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mEndingDialog != null) {
			mEndingDialog.dismiss();
		}
		if (m_interView != null) {
			m_interView.onDestroy();
			m_interView = null;
		}

	}

	private View.OnClickListener leftClickListener2 = new View.OnClickListener() {
		public void onClick(View v) {
			if (mEndingDialog != null) {
				mEndingDialog.dismiss();
				mEndingDialog = null;
			}
			finish();
		}

	};
	private View.OnClickListener rightClickListener2 = new View.OnClickListener() {
		public void onClick(View v) {
			if (mEndingDialog != null) {
				mEndingDialog.dismiss();
				mEndingDialog = null;
			}

		}

	};

	// 광고 유료/무료
	public void onChargeableBannerType(View v, boolean bcharge) {
		Utils.onChargeableBannerType(bcharge);
	}

	public void onFailedToReceive(View v, int errorCode) {
		Utils.customErrorMsg(errorCode, new Handler() {
			@Override
			public void dispatchMessage(Message msg) {
				String log = String.valueOf(msg.obj);
				Utils.log(log);
			}
		});
	}

	// 전면 광고 닫기 이벤트
	public void onInterClose(View v) {
		Utils.log("전면 광고 닫기");
	}

	public void onAdClick(View v) {
		// TODO Auto-generated method stub

	}

}
