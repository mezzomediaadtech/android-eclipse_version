package com.mplus.sdk.sample.release;

import com.mapps.android.share.AdInfoKey;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class Utils {
	public final static String LOGTAG = "MZTEST";
	
	public static void debug(Context context, String msg) {
		Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
		log(msg);
	}

	public static void log(String msg) {
		Log.d(LOGTAG, msg);
	}
	
	public static void customErrorMsg(int errorCode, Handler handler) {
		Message msg = new Message();
		switch (errorCode) {
		case AdInfoKey.AD_SUCCESS:
			msg.obj = "[ " + errorCode + " ] " + "광고 성공";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.NETWORK_ERROR:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)네트워크";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_ADCLICK:
			msg.obj = "[ " + errorCode + " ] " + "광고 이벤트 클릭";
			handler.dispatchMessage(msg);
			break;			
		case AdInfoKey.AD_SERVER_ERROR:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)서버";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_API_TYPE_ERROR:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)API 형식 오류";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_APP_ID_ERROR:
		case AdInfoKey.AD_WINDOW_ID_ERROR:
		case AdInfoKey.AD_ID_BAD:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)ID 오류";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_ID_NO_AD:
			msg.obj = "[ " + errorCode + " ] " + "광고 소진";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_CREATIVE_ERROR:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)광고 생성 불가";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_ETC_ERROR:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)예외 오류";
			handler.dispatchMessage(msg);
			break;

		case AdInfoKey.AD_INTERVAL:
			msg.obj = "[ " + errorCode + " ] " + "광고 요청 어뷰징";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_TIMEOUT:
			msg.obj = "[ " + errorCode + " ] " + "광고 API TIME OUT";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.NOT_UNZIP_TIMEOUT:
			msg.obj = "[ " + errorCode + " ] " + "3D 광고 TIME OUT";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.NOT_SUPPORT_VERSION:
			String releaseVersion = android.os.Build.VERSION.RELEASE;
			int apilevel = android.os.Build.VERSION.SDK_INT;
			msg.obj = "[ " + errorCode + " ] " + "3D나 전면 미지원 버전(release version : " + releaseVersion + " /api level : " + apilevel + " )";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.NOT_SUPPORTED_DEVICE:
			msg.obj = "[ " + errorCode + " ] " + "3D나 전면 미지원 디바이스";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.CREATIVE_FILE_ERROR:
			msg.obj = "[ " + errorCode + " ] " + "(ERROR)파일 형식";
			handler.dispatchMessage(msg);
			break;
		case AdInfoKey.AD_BACKGROUND_CALL
				// wrong call
			break;
		default:
			msg.obj = "[ " + errorCode + " ] " + "etc";
			handler.dispatchMessage(msg);
			break;
		}
	}
	
	public static void onChargeableBannerType(boolean bcharge) {
		if (bcharge) {
			Utils.log("-------> chargeable advertise !!!");
		} else {
			Utils.log("-------> Non chargeable advertise !!");
		}
	}
}
