package com.mplus.sdk.sample.release;

import com.mapps.android.share.AdInfoKey;
import com.mapps.android.view.AdVideoPlayer;
import com.mz.common.listener.AdVideoPlayerErrorListener;
import com.mz.common.listener.AdVideoPlayerListener;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.VideoView;

public class PlayerSample extends Activity implements AdVideoPlayerListener, AdVideoPlayerErrorListener {
	AdVideoPlayer adPlayer = null;
	private Handler handler = new Handler();
	private VideoView m_videoView = null;
	private boolean bActivate = true;
	private LinearLayout rootVideo;
	private GestureDetector mGesture;
	private int mScreenMode = AdVideoPlayer.MODE_WIDE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.layout_player);
		makeDoubleTab();
		m_videoView = (VideoView) findViewById(R.id.videoViewExample);
		rootVideo = (LinearLayout) findViewById(R.id.ad_video_area);
		rootVideo.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			public boolean onTouch(View v, MotionEvent event) {
				mGesture.onTouchEvent(event);
				return true;
			}
		});
		String p = getIntent().getStringExtra(Constants.P);
		String m = getIntent().getStringExtra(Constants.M);
		String s = getIntent().getStringExtra(Constants.S);
		// aplusPlay();
		createJavaPlay(p, m, s);
	}

	// private void aplusPlay(){
	// adPlayer = (AdVideoPlayer) findViewById(R.id.ad_player);
	// if(adPlayer!=null){
	// adPlayer.setAdViewCode(p, m, s);
	// adPlayer.setCateContent("", "");
	// adPlayer.setUserAge("1");
	// adPlayer.setUserGender("2");
	// adPlayer.setLoaction(true);
	// adPlayer.setAccount("id");
	// adPlayer.setEmail("few.com");
	// adPlayer.setAdVideoPlayerErrorListner(this);
	// adPlayer.setAdVideoPlayerListner(this);
	// adPlayer.setVideoMode(AdVideoPlayer.MODE_WIDE);
	// adPlayer.showAd();
	// }
	// }

	private void createJavaPlay(String p, String m, String s) {
		rootVideo.removeAllViews();
		rootVideo.setVisibility(View.VISIBLE);
		adPlayer = new AdVideoPlayer(PlayerSample.this);
		adPlayer.setAdViewCode(p, m, s);
		adPlayer.setUserAge("1");
		adPlayer.setUserGender("2");
		adPlayer.setLoaction(true);
		adPlayer.setAccount("id");
		adPlayer.setEmail("few.com");
		adPlayer.setUserAgeLevel("0");
		adPlayer.setStoreurl("http://store.url");
		adPlayer.setKeyword("");
		adPlayer.setExternal("");
		adPlayer.setAdVideoPlayerErrorListner(this);
		adPlayer.setAdVideoPlayerListner(this);
		adPlayer.setVideoMode(AdVideoPlayer.MODE_WIDE);
		rootVideo.addView(adPlayer);
		adPlayer.showAd();
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		resize();
		super.onWindowFocusChanged(hasFocus);
	}

	private void resize() {
		DisplayMetrics dm = getApplicationContext().getResources().getDisplayMetrics();
		int dw = dm.widthPixels;
		int dh = dm.heightPixels;
		int holderWidth = 0;
		int holderHeight = 0;
		switch (mScreenMode) {
		case AdVideoPlayer.MODE_NORMAL: // 4:3
			holderWidth = dw;
			holderHeight = ((dw / 4) * 3);
			if (holderHeight > dh) {
				holderHeight = dh;
				holderWidth = ((dh / 3) * 4);
			}
			break;
		case AdVideoPlayer.MODE_WIDE: // 16:9
			holderWidth = dw;
			holderHeight = ((dw / 16) * 9);
			if (holderHeight > dh) {
				holderHeight = dh;
				holderWidth = ((dh / 9) * 16);
			}
			break;
		case AdVideoPlayer.MODE_STRETCH:
			holderWidth = dw;
			holderHeight = dh;
			break;
		case AdVideoPlayer.MODE_ORIGNAL:

			break;
		}
		FrameLayout.LayoutParams fParams = new FrameLayout.LayoutParams(holderWidth, holderHeight, Gravity.CENTER);
		rootVideo.setLayoutParams(fParams);
		m_videoView.setLayoutParams(fParams);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (adPlayer != null)
			adPlayer.onDestory();

		rootVideo.removeAllViews();
		adPlayer = null;
	}

	@Override
	protected void onPause() {
		super.onPause();
		bActivate = false;
		if (adPlayer != null)
			adPlayer.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!bActivate) {
			if (adPlayer != null)
				adPlayer.onResume();
		}
	}

	@TargetApi(Build.VERSION_CODES.ECLAIR)
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		if (adPlayer != null)
			adPlayer.onBackPressed();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		switch (newConfig.orientation) {
		case Configuration.ORIENTATION_PORTRAIT:
			resize();
			break;
		case Configuration.ORIENTATION_LANDSCAPE:
			resize();
			break;

		}
	}

	public void onError(MediaPlayer mp, int what, int extra) {
		final int nwhat = what;
		handler.post(new Runnable() {
			public void run() {
				Utils.log("onError=" + nwhat);
				eventAction();
			}

		});
	}

	public void onAdPlayerReceive(View view, int recvCode) {
		if (adPlayer != null) {
			if (adPlayer == view) {
				final int errcode = recvCode;
				handler.post(new Runnable() {
					public void run() {
						setMsg(errcode);
					}

				});
			}
		}
	}

	private void setMsg(int recvCode) {
		switch (recvCode) {
		///////////////////////////////////////////////////////////////////////////////////
		case AdInfoKey.AD_SUCCESS:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "성공");
			rootVideo.setVisibility(View.VISIBLE);
			break;

		case AdInfoKey.AD_PLAYER_COMPLETE:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "영상 광고 완료");
			eventAction();
			break;
		case AdInfoKey.AD_PLAYER_AD_START:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "영상광고 시작");
			break;
		case AdInfoKey.AD_PLAYER_SKIP:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "영상광고 스킵");
			eventAction();
			break;
		case AdInfoKey.AD_ADCLICK:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "광고 클릭");
			eventAction();
			break;
		case AdInfoKey.AD_PLAYER_INCOMING_CALL:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "전화 중...");
			eventAction();
			break;
		case AdInfoKey.AD_PLAYER_FINISH_OTHER_REASON:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "예외상황 영상 종료");
			eventAction();
			break;

		case AdInfoKey.AD_PLAYER_AD_LOGO_CLICK:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "영상 로고 클릭");
			eventAction();
			break;
		////////////////////////////////////////////////////////////////////////////////////
		// error
		case AdInfoKey.NETWORK_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)네트워크");
			eventAction();
			break;
		case AdInfoKey.AD_SERVER_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)서버");
			eventAction();
			break;
		case AdInfoKey.AD_API_TYPE_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)API 형식 오류");
			eventAction();
			break;
		case AdInfoKey.AD_APP_ID_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)ID 오류");
			eventAction();
			break;
		case AdInfoKey.AD_WINDOW_ID_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)ID 오류");
			eventAction();
			break;
		case AdInfoKey.AD_ID_BAD:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)ID 오류");
			eventAction();
			break;
		case AdInfoKey.AD_ID_NO_AD:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "광고 소진");
			eventAction();
			break;
		case AdInfoKey.AD_CREATIVE_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)광고 생성 불가");
			eventAction();
			break;
		case AdInfoKey.CREATIVE_FILE_ERROR:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "(ERROR)영상 파일 오류");
			eventAction();
			break;
		case AdInfoKey.AD_INTERVAL:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "어뷰징");
			eventAction();
			break;
		case AdInfoKey.AD_TIMEOUT:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "광고 API TIME OUT");
			eventAction();
			break;
		default:
			Utils.debug(PlayerSample.this, "[ " + recvCode + " ] " + "ETC");
			break;
		}
	}

	public void onAdPlayerDurationReceive(View arg0, int sec) {
		Utils.log(sec / 1000 + "초");
		new Thread(new Runnable() {
			public void run() {
				if (adPlayer != null) {
					while (adPlayer != null && adPlayer.getVideoCurrentDuration() != -1) {
						// 총 광고소재물 재생 시간 가져오기
						Utils.log((adPlayer.getVideoCurrentDuration() / 1000) + "sec");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}).start();
	}

	private void eventAction() {
		if (m_videoView != null) {
			String mediaURL = "http://vod.midas-i.com/20131010171708_middle.mp4";
			m_videoView.setVideoURI(Uri.parse(mediaURL));
			rootVideo.setVisibility(View.GONE);
			m_videoView.setVisibility(View.VISIBLE);
			m_videoView.start();
		}
	}

	private void makeDoubleTab() {
		mGesture = new GestureDetector(getApplicationContext(), new GestureDetector.SimpleOnGestureListener() {
			@Override
			public boolean onDoubleTap(MotionEvent e) {
				if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // 가로전환
				} else {
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); // 가로전환
				}
				return super.onDoubleTap(e);
			}
		});
	}
}
